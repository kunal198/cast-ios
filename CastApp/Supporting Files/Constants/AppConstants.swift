//
//  AppConstants.swift
//  Roasted
//
//  Created by Manish Gumbal on 16/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation


//****************************   MARK: Data Manager Constants    ***********************
let kUserId = "kUserId"
let kFName = "kFName"
let kLName = "kLName"

let kUserImage = "kUserImage"
let kUserEmail = "kUserEmail"
let kIsFBLoggedIn = "kIsFBLoggedIn"
let kDeviceToken = "kDeviceToken"
let kVerificationCode = "kVerificationCode"

let kIsCastee = "kIsCastee"
let kIsNotificationOn = "kIsNotificationOn"
let kIsUserLoggedIn = "kIsUserLoggedIn"
let kPatientList = "kPatientList"



//****************************   MARK: Common Constants    ***********************
let BASE_API_URL = "http://api.healthsplash.com/"


let BASE_IMAGE_URL = "http://14.141.82.35/Server10/roastapp/public/upload"
let BASE_USER_URL = "http://14.141.82.35/Server10/roastapp/public/upload/user_image"
let BASE_ROAST_URL = "http://14.141.82.35/Server10/roastapp/public/upload/post_image"

//MARK: Google API key
let GOOGLE_API_KEY = "AIzaSyD5LsOj6dBm4R1fm0rygc797qW62Iw8f-A"

//let GOOGLE_API_KEY = "AIzaSyD-RILx7SB4la4eHG5_J8mTs6yNxXuFgL0"

//MARK: Color
let kTextColor = "555555"
let kLightPurple = "574155"
let kPurple = "3C273A"
let kStatusBarColor = "C4C4C4"
let kLightGray = "7A7A7A"
let KWhiteColor = "D2D2D0"

let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

//MARK:
//MARK: Navigation Title


//MARK:
//MARK: TAbleViewCell

let kCell                   = "Cell"
let kGigsCell               = "GigsCell"
let kPhotoCell              = "PhotoCell"
let kVideoCell              = "VideoCell"

let kHeaderCell             = "HeaderCell"
let kAppointmentDetailCell  = "AppointmentDetailCell"


// MARK: Logout String

let kLogoutString   = "Are you sure you want to logout?"

// MARK:
// MARK:Alert Action 

let kYes   =  "Yes"
let kNo    =  "No"
let kOk    =  "Ok"

// MARK:
// MARK:ViewControllers

let kLeftSlideVC            = "LeftSlideVC"
let kNotificationVC         = "NotificationVC"
let kLoginVC                = "LoginVC"
let kCustomTabbarVC         = "CustomTabBarVC"
let kLoginNavigationVC      = "LoginNavigationVC"

//MARK: Titles

let kFindCastee             = "Find Castee"
let kFindGigs               = "Find Gigs"


// MARK: Strings
// Home
let kAvailableGigs          = "Available Gigs"
let kReccomendedCastee      = "Reccomended Castees"


//MARK:
//MARK: Segue Constants

//LOGIN
let kLoginToForgot           = "loginToForgot"
let kLoginToSignUP           = "loginToSignUp"
let kLoginToVarification     = "loginToVerification"
let kFindCasteeToCalender    = "FindCasteeToCalender"


//SIGNUP
let kSignUpToCreateEmployer       = "SignUpToCreateEmployer"


// GIGS

let kGigsToViewGigs        = "GigsToViewGig"
let kGigToScheduleGig      = "gigToScheduleGig"



//APPLICATION VC

let kApplicationToAddVideoQuestion = "applicationToAddVideoQuestion"

//VIEW GIG
let kViewgigToCastVC        = "ViewgigToCastVC"
let kViewGigToViewQuestions        = "ViewGigToViewQuestions"




//PROFILE

let kProfileToEdit           = "ProfileToEdit"

//REVIEWS AND RATING

let kReviewsToEditReviews    = "reviewsToEditReviews"

//PATIENT
let kPatientsToAddPatient    = "patientsToAddPatient"
let kPatientsToEdit          = "patientsToEdit"

//EDIT PATIENT

let kEditPatientToInsurance = "EditPatientToInsurance"

// ADD PATIENT

let kAddPatientToInsurance  = "AddPatientToInsurance"


//MESSAGES

let kEnterEmail                 = "Please enter your email address."
let kEnterPassword              = "Please enter password."
let kEnterValidEmail            = "Please enter valid email address."
let kEnterFirstName             = "Please enter your first name."
let kEnterPatientFirstName      = "Please enter patient's first name."
let kEnterLastName              = "Please enter your last name."
let kEnterPatientLastName       = "Please enter patient's last name."
let kSelectInsurance            = "Please select insurance first."
let kEnterConfirmPassword       = "Please enter confirm password."
let kFirstNameLength            = "First name needs to be at least 3 characters."
let kLastNameLength             = "Last name needs to be at least 3 characters."
let kPasswordNeeds8Character    = "Password must contain 8 characters with at least 1 number."
let kPasswordAndConfirmPassword = "Your password and confirm password does not match."
let kAgreeWithTerms             = "Please agree with Terms & Condition."
let kSelectSpeciality           = "Please select the speciality."
let kSelectAppointmentType      = "Please select the appointment type."
let kSelectCurrentLocation      = "Please select your current location."
let kSelectPatient              = "Please select the patient."





