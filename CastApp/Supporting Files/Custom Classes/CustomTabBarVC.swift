//
//  CustomTabBarVC.swift
//  HealthSplashPatient
//
//  Created by 123 on 18/04/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import UIKit


class CustomTabBarVC: UITabBarController {
    
  //VARIABLES
    var casterProfileVC:CasterProfileVC!
    var profileVc:ProfileVC!
    var settingsVC:SettingsVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: customizeUI
    func customizeUI()  {
        
        let storyboard = UIStoryboard(storyboard: .Profile)
        casterProfileVC = storyboard.instantiateViewController(withIdentifier: "CasterProfileVC") as! CasterProfileVC
        profileVc = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        checkUserRole()
        // Define identifier
        let notificationName = Notification.Name("checkUserRole")
        // remove notification
        NotificationCenter.default.removeObserver(self, name: notificationName, object: nil);
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(CustomTabBarVC.checkUserRole), name: notificationName, object: nil)
        
        
        // Change the 5s tababr bg image
        if UIScreen.main.bounds.size.height <= 568{
            self.tabBar.backgroundImage = #imageLiteral(resourceName: "bg5s")
        }
        
        
    }
    
    
 //MARK: ChangeVC Method
  func checkUserRole(){
    if DataManager.isCastee == false {
               //
        if var vcArray = self.viewControllers {
            //Arrange the array according to your need and set them again.
            vcArray.remove(at: 2)
            vcArray.insert(casterProfileVC, at: 2)

            self.viewControllers = vcArray
            self.tabBar.items![2].image = #imageLiteral(resourceName: "Profile")
            self.tabBar.items![2].imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        }
    }else{

        if var vcArray = self.viewControllers {
            //Arrange the array according to your need and set them again.
            vcArray.remove(at: 2)
            vcArray.insert(profileVc, at: 2)
            self.viewControllers = vcArray
            self.tabBar.items![2].image = #imageLiteral(resourceName: "Profile")
            self.tabBar.items![2].imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);

         }
       }
    }
    
    //MARK: - Alert/Error/Notification Implementation
    func showAlert(message: String?, title:String? , otherButtons:[String:((UIAlertAction)-> ())]? = nil, cancelTitle: String = "OK", cancelAction: ((UIAlertAction)-> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction))
        
        if otherButtons != nil {
            for key in otherButtons!.keys {
                alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons![key]))
            }
        }
        present(alert, animated: true, completion: nil)
    }
    
    func logout()  {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        let mainStoryboard = UIStoryboard(storyboard: .Main)
        //assign root view controller
        let rootController = mainStoryboard.instantiateViewController(withIdentifier: kLoginNavigationVC)
        // Because self.window is an optional you should check it's value first and assign your rootViewController
        if let window = UIApplication.shared.delegate?.window{
            window?.layer.add(transition, forKey: nil)
            window?.rootViewController = rootController
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

