//
//  SCSkypeActivityIndicatorView.h
//  SCSkypeActivityIndicatorView
//
//  Created by DEFTSOFT on 03/04/17.
//  Copyright © 2017 Stefan Ceriu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCSkypeActivityIndicatorView : UIView

@property (nonatomic, assign) NSUInteger numberOfBubbles;

@property (nonatomic, strong) UIColor *bubbleColor;

@property (nonatomic, assign) CGSize bubbleSize;

@property (nonatomic, assign) NSTimeInterval animationDuration;

- (void)startAnimating;
- (void)stopAnimating;
- (BOOL)isAnimating;
- (instancetype)init;
- (instancetype)initWithFrame:(CGRect)frame;


@end
