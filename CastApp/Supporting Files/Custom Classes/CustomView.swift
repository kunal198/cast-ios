//
//  CustomView.swift
//  HealthSplashPatient
//
//  Created by 123 on 14/04/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import UIKit

class CustomView: UIView {

    override init (frame : CGRect) {
        super.init(frame : frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    override func draw(_ rect: CGRect) {
    }
    
    func setUpView()  {
        //adding gesture to image view from zooming over the UIWindow
        self.layer.cornerRadius = 5
        self.layer.borderColor = UIColor.HealthSplashPatientColor.gray.color(with: 0.8).cgColor
        self.layer.borderWidth = 0.5
    }

   

}
