//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }

    func dateFromString(format: DateFormat) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        //debugPrint("End date in date from string method \(dateFormatter.date(from: self))")
        return dateFormatter.date(from: self)
    }
    
        func fromBase64() -> String? {
            guard let data = Data(base64Encoded: self) else {
                return nil
            }
            
            return String(data: data, encoding: .utf8)
        }
        
        func toBase64() -> String {
            return Data(self.utf8).base64EncodedString()
        }
    
}
