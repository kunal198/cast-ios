//
//  UIButton.swift
//  Roasted
//
//  Created by Manish Gumbal on 13/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func setUnderLine() {
        let titleString = NSMutableAttributedString(string: self.titleLabel!.text!)
        titleString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, self.titleLabel!.text!.characters.count))
        self.setAttributedTitle(titleString, for: .normal)
    }
    
    func setButtonBorderLayer()  {
        self.layer.cornerRadius = 52.5
        self.layer.borderColor = UIColor.HealthSplashPatientColor.gray.color(with: 0.8).cgColor
        self.layer.borderWidth = 0.5
    }
    func setButtonGrayBorderLayer(radius:CGFloat,alpha:CGFloat)  {
        self.layer.cornerRadius = radius
        self.layer.borderColor = UIColor.HealthSplashPatientColor.grayWhite.color(with: alpha).cgColor
        self.layer.borderWidth = 0.8
    }
    func setBorderCornerRadius(radius: CGFloat)  {
        self.layer.cornerRadius = radius
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
    }
}
