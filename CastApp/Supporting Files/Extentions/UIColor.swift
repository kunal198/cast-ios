//
//  UIColor.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIColor {

    convenience init(hex: String ,Alpha:CGFloat) {
        self.init(hex: hex, alpha:Alpha)
    }

    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
    
    enum HealthSplashPatientColor  {
        
        case pink
        case gray
        case lightPurple
        case grayWhite
       
        
        func color(with alpha: CGFloat) -> UIColor {
            
            var colorToReturn:UIColor?
            
            switch self {
                
            case .pink:
                colorToReturn = UIColor(red: 238/255, green: 108/255, blue: 122/255, alpha: alpha)
                
            case .lightPurple:
                colorToReturn = UIColor(red: 87/255, green: 65/255, blue: 85/255, alpha: alpha)

            case .gray:
                colorToReturn = UIColor.lightGray.withAlphaComponent(alpha)
                
            case .grayWhite:
                colorToReturn = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: alpha)
            }
            
            return colorToReturn!
        }
    }

    
}





