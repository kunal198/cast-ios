//
//  Date.swift
//
//
//  Created by Prabhjot Singh on 16/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//06/23/2017 06:32 AM

import Foundation
enum DateFormat: String {
    case dateTime  = "MM/dd/yyyy hh:mm a"
    case date      = "MM/dd/yyyy"
    case dateTimeT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case mdyDate   = "MM-dd-yyyy"
    case dMyDate   = "dd-MMMM-yyyy-hh:mm a"
    case dmyDate   = "dd-MMMM yyyy"
    case time      = "hh:mm a"
}

//func convertDateFormater(date: String) -> String {
//    let dateFormatter = NSDateFormatter()
//    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//    dateFormatter.timeZone = NSTimeZone(name: "UTC")
//    
//    guard let date = dateFormatter.dateFromString(date) else {
//        assert(false, "no date from string")
//        return ""
//    }
//    
//    dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"
//    dateFormatter.timeZone = NSTimeZone(name: "UTC")
//    let timeStamp = dateFormatter.stringFromDate(date)
//    
//    return timeStamp
//}

//please check: date is not appearing in proper format
extension Date {
    func stringFromDate(format: DateFormat) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        
        //check for local time zome
        if format == .mdyDate {
           // debugPrint("End date in fomatter method \(stringFormDateInLocalTimeZone(format: format))")
            return stringFormDateInLocalTimeZone(format: format)
        }
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        debugPrint(dateFormatter.string(from: self))
        return dateFormatter.string(from: self)
    }
    
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func stringFormDateInLocalTimeZone(format: DateFormat) -> String{
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.local        // or as local time
        formatter.dateFormat = format.rawValue
        
      //  debugPrint(formatter.string(from: self))
        return formatter.string(from: self)
    }
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    func dayOfWeek() -> String? {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!        // or as local time
            dateFormatter.dateFormat = "EEEE"
            return dateFormatter.string(from: self).capitalized
            // or use capitalized(with: locale) if you want
        }
    
}
