//
//  UIViewController.swift
//  Focusfied
//
//  Created by 123 on 17/01/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func setNavigationBarItem() {
      //  self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
      // self.addRightBarButtonWithImage(UIImage(named: "icon-hamberger")!)
       self.slideMenuController()?.removeLeftGestures()
       self.slideMenuController()?.removeRightGestures()
       self.slideMenuController()?.addLeftGestures()
       self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
}
