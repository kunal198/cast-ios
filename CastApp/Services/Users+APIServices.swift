//
//  Users+APIServices.swift
//  HealthSplashPatient
//
//  Created by 123 on 13/04/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import Foundation


enum UserAPIServices: APIService {
    
    case SignUpByEmail(signupDetails: NSDictionary)
    case LoginByEmail(loginDetails: NSDictionary)
    case SocialLogin(socialLoginDetails:NSDictionary)
    case ForgotPassword(forgotPasswordDetails: NSDictionary)
    case FetchInsuranceList()
    case EditProfile(editProfileDetails: NSDictionary)
    case VerifyEmail(verificationDetails: NSDictionary)

    var path: String {
        var path = ""
        switch self {
            
        case .SignUpByEmail:
            path = BASE_API_URL.appending("user")
        case .LoginByEmail:
            path = BASE_API_URL.appending("login")
        case .SocialLogin:
            path = BASE_API_URL.appending("login/social")
        case .ForgotPassword:
            path = BASE_API_URL.appending("forgotpassword")
        case .FetchInsuranceList:
            path = BASE_API_URL.appending("insurance")
        case .EditProfile:
            path = BASE_API_URL.appending("user")
        case .VerifyEmail:
            path = BASE_API_URL.appending("user/verify")

        }
        return path
    }
    
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .SignUpByEmail(signupDetails):
            resource = Resource(method: .post, parameters: signupDetails as? [String : Any], headers: nil)
            
        case let .LoginByEmail(loginDetails):
            resource = Resource(method: .post, parameters: loginDetails as? [String : Any] , headers: nil)
            
        case let .SocialLogin(socialLoginDetails):
            resource = Resource(method: .post, parameters: socialLoginDetails as? [String : Any] , headers: nil)

        case let .ForgotPassword(forgotPasswordDetails):
            resource = Resource(method: .get, parameters: forgotPasswordDetails as? [String : Any] , headers: nil)
            
        case .FetchInsuranceList():
            resource = Resource(method: .get, parameters: nil  , headers: nil)
            
        case let .EditProfile(editProfileDetails):
            resource = Resource(method: .post, parameters: editProfileDetails as? [String : Any] , headers: nil)
            
        case let .VerifyEmail(verificationDetails):
            resource = Resource(method: .get, parameters: verificationDetails as? [String : Any] , headers: nil)
            
            
    }
        return resource
    }
}


extension APIManager {
    class func signupByEmail(signupDetails: NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.SignUpByEmail(signupDetails: signupDetails).uploadMultiple(imageDict: nil,success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func loginWithEmail(loginDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.LoginByEmail(loginDetails:loginDetails).uploadMultiple(imageDict: nil, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func socialLogin(socialLoginDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.SocialLogin(socialLoginDetails:socialLoginDetails).uploadMultiple(imageDict: nil, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func forgotPassword(forgotPasswordDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.ForgotPassword(forgotPasswordDetails:forgotPasswordDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func fetchInsuranceList(successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.FetchInsuranceList().request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func editProfile(editProfileDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.EditProfile(editProfileDetails: editProfileDetails).uploadMultiple(imageDict: nil, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func verifyEmail(verificationDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.VerifyEmail(verificationDetails: verificationDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }

}

