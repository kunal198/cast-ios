//
//  UserDataModel.swift
//  Demo
//
//  Created by 123 on 13/07/17.
//  Copyright © 2017 TBI. All rights reserved.
//

import Foundation

struct InsuranceList {  //Insurance List 
    var id: String?
    var provider: String?
  
}


extension UserVM {
    func parseUserData(responseDict: JSONDictionary!, type: String) {
        var data: JSONDictionary?
        let dataArray = responseDict[APIKeys.kData] as? NSArray
        data = dataArray?[0] as? JSONDictionary
        if data != nil {
            DataManager.userId              = (data![APIKeys.kId] as! Int)
            DataManager.email               = data![APIKeys.kUserName] as? String
         
            DataManager.patientList         = data![APIKeys.kPatient] as? [NSDictionary]
        }
    }
}
