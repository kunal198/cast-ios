//
//  BaseViewController.swift
//  Demo
//
//  Created by 123 on 05/07/17.
//  Copyright © 2017 123. All rights reserved.
//

import UIKit



//MARK: Base Protocol
@objc protocol  BaseVCDelegate {
    @objc optional func didSelectPickerViewAtIndex(index: Int)
    @objc optional func didSelectDatePicker(date: Date)
    @objc optional func didClickOnDoneButton()
}

protocol CustomImagePickerDelegate {
    func didPickImage(_ image: UIImage)
}

class BaseViewController: UIViewController {

    //MARK: Variables
    var imagePickerDelegate: CustomImagePickerDelegate?
    var datePickerView: UIDatePicker!
    var pickerDelegate: BaseVCDelegate?
        
    var pickerArray = [String]()
    var pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        debugPrint("MEMORY WARNING")
    }
   
    

    
    //MARK: *************  Navigation Bar and Status Bar Methods  ************
    
    func setStatusBarLight() {
        //Set Status Bar Color
          UIApplication.shared.statusBarStyle = .lightContent
          UIApplication.shared.isStatusBarHidden = false

    }
    func setStatusBarDark()  {
        UIApplication.shared.statusBarStyle = .default
        UIApplication.shared.isStatusBarHidden = false
    }
    
    func setTitle(title:String) {   //Set Navigation Bar Title With Background Color
        self.navigationController?.isNavigationBarHidden = false
        _ = self.navigationController?.navigationBar.barTintColor = UIColor.HealthSplashPatientColor.lightPurple.color(with: 1.0)
        
     //  self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
     //   self.navigationController?.navigationBar.layer.shadowOffset = .zero
      //  self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
      //  self.navigationController?.navigationBar.layer.shadowRadius = 10.0
        
        let titleLabel = UILabel(frame: CGRect(x: 50, y:5, width:100, height:18))
        titleLabel.textAlignment = .center
        titleLabel.text = title
        titleLabel.textColor = .white
        titleLabel.font = UIFont.HealthSplashFont.regular.fontWithSize(size: 15)
        self.navigationItem.titleView = titleLabel
//        self.navigationController?.navigationBar.setTitleVerticalPositionAdjustment(CGFloat(15), for: UIBarMetrics.default)

    }
    
    func setBackButton(){
        let backButton = UIButton() //Custom back Button
        backButton.frame = CGRect(x: 0, y: 0, width: 42, height: 36)
        backButton.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        backButton .addTarget(self, action: #selector(self.backButtonAction), for: UIControlEvents.touchUpInside)
        let leftBarButton = UIBarButtonItem()
        leftBarButton.customView = backButton
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -15;
        self.navigationItem.setLeftBarButtonItems([negativeSpacer, leftBarButton], animated: false)
    }
   
    //Back Button Action
    func backButtonAction() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func popToRootVC() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popToRootViewController(animated: false)
    }
    
    
    //MARK:
    //MARK: Set Home as Root with Animation
    func loginToHomeVC()  {
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        let mainStoryboard = UIStoryboard(storyboard: .Main)
        //assign root view controller
        let rootController = mainStoryboard.instantiateViewController(withIdentifier: kCustomTabbarVC)
        // Because self.window is an optional you should check it's value first and assign your rootViewController
        if let window = UIApplication.shared.delegate?.window{
            window?.layer.add(transition, forKey: nil)
            window?.rootViewController = rootController
        }
    }
    
    func logout()  {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        let mainStoryboard = UIStoryboard(storyboard: .Main)
        //assign root view controller
        let rootController = mainStoryboard.instantiateViewController(withIdentifier: kLoginNavigationVC)
        // Because self.window is an optional you should check it's value first and assign your rootViewController
        if let window = UIApplication.shared.delegate?.window{
            window?.layer.add(transition, forKey: nil)
            window?.rootViewController = rootController
        }
    }

    
    
    func animationPushWithIdentifier(identifier:String){
        
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        performSegue(withIdentifier: identifier, sender: nil)
        
    }
    func animationPushWithViewController(viewController:UIViewController){
        
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }

    func hideNavigationBar()  {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func showNavigationBar()  {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

//MARK:
//MARK: Tabbar controller methods
    func hideTabbar() {
        self.tabBarController?.tabBar.isHidden = true
    }
    func showTabbar() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func showImagePicker() {
        
        CustomImagePickerView.sharedInstace.delegate = self
        let alert  = UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: {action in
            CustomImagePickerView.sharedInstace.pickImageUsing(target: self, mode: .gallery)
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {action in
            CustomImagePickerView.sharedInstace.pickImageUsing(target: self, mode: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openLoginVC()  {
        
        let mainStoryboard = UIStoryboard(storyboard: .Main)
        let loginVC = mainStoryboard.instantiateViewController(withIdentifier: kLoginVC)
        animationPushWithViewController(viewController: loginVC)
    }
    
//MARK:
//MARK: Set Picker View 
    func setPickerView(textField: UITextField, array: [String]) {
        //Set Picker View Delegates
        pickerView.delegate = self
        pickerArray = array
        //Set Toolbar for Picker View
        let toolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 44.0))
        let item = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done,
                                   target: self, action: #selector(BaseViewController.doneAction))
        //Set Picker View UI
        item.tintColor = UIColor.white
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        toolbar.setItems([flexible,item], animated: true)
        pickerView.backgroundColor = UIColor.white
        toolbar.barTintColor = UIColor.HealthSplashPatientColor.lightPurple.color(with: 1.0)
        textField.inputAccessoryView = toolbar

        //Set Picker View to Textfield
        textField.inputView = pickerView
        textField.tintColor = .clear
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }
    
//MARK: Toolbar Acions
    func doneAction() {
        pickerDelegate?.didClickOnDoneButton?()
        view.endEditing(true)
    }
//MARK: *************   Set Date Picker   ***************
    
    func setDatePickerView(textField: UITextField ,mode:Int) {
        datePickerView = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode(rawValue: mode)!
        datePickerView.backgroundColor = UIColor.init(hex: kPurple, Alpha: 1.0)
        datePickerView.setValue(UIColor.white, forKeyPath: "textColor")
       
        let toolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 44.0))
        let item = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done,
                                   target: self, action: #selector(BaseViewController.doneAction))
        //Set Picker View UI
        item.tintColor = UIColor.white
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        toolbar.setItems([flexible,item], animated: true)
        toolbar.barTintColor = UIColor.HealthSplashPatientColor.lightPurple.color(with: 1.0)
        textField.inputAccessoryView = toolbar
        textField.tintColor = .clear

       // datePickerView.maximumDate = Date()
      //  let date = "01-01-1950".dateFromString(format: .dmyDate)
        let date = Date().stringFormDateInLocalTimeZone(format: .dateTime)
        datePickerView.minimumDate = Date()
        datePickerView.date = date.dateFromString(format: .dateTime)!
        datePickerView.timeZone = TimeZone(abbreviation: "GMT")
        textField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.didPickerViewValueChanged(sender:)), for: .valueChanged)
    }
  
    
}

//MARK: *************   Custom Image Picker Delegates   ***************
extension BaseViewController: CustomPickerViewDelegate {
    
    func didImagePickerFinishPicking(_ image: UIImage) {
        imagePickerDelegate?.didPickImage(image)
    }
    
    func didCancelImagePicking() {
        
    }
}
//MARK:
//MARK: Picker View Delegates
extension BaseViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerDelegate?.didSelectPickerViewAtIndex?(index: row) //Call Protocol Delegate
    }
}

extension BaseViewController {
    func didPickerViewValueChanged(sender: UIDatePicker) {
        pickerDelegate?.didSelectDatePicker?(date: sender.date)
    }
}

//MARK:
//MARK: Alert View Controller Delegates
    extension BaseViewController {
        
//MARK: - Alert/Error/Notification Implementation
        func showAlert(message: String?, title:String? , otherButtons:[String:((UIAlertAction)-> ())]? = nil, cancelTitle: String = "OK", cancelAction: ((UIAlertAction)-> ())? = nil) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction))
            
            if otherButtons != nil {
                for key in otherButtons!.keys {
                    alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons![key]))
                }
            }
            present(alert, animated: true, completion: nil)
        }
        
        func showErrorMessage(error: NSError, cancelAction: ((UIAlertAction)-> ())? = nil) {
            let alert = UIAlertController(title: error.domain, message: error.userInfo[kMessage] as? String ?? "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: cancelAction))
            present(alert, animated: true, completion: nil)
        }
//MARK:
//MARK: Show toast on click
        func showToastWithMessage(message: String) {
            let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
//MARK:
//MARK: Conversion Methods
        func secondsToMinutes (seconds : Int) -> ( Int) {
             return ( (seconds % 3600) / 60)
        }
        
        func secondsToSeconds (seconds : Int) -> ( Int) {
             return ( (seconds % 3600) % 60)
        }
        func convertDateFromString(date:String) -> (Date)  {
             let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
             let s = dateFormatter.date(from: date)
             return s!
        }
        
//MARK:
//MARK: Change Status bar color
        func setStatusBarBackgroundColor(color: UIColor) {
             guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
             statusBar.backgroundColor = color
        }
        
//MARK:
//MARK: Name validation method
        func validateNameTextFeild(string:String) -> Bool {
             let set = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS)
             let inverted = set.inverted
             let filtered = string.components(separatedBy:inverted).joined(separator: "")
             return filtered == string;
        }
    }
