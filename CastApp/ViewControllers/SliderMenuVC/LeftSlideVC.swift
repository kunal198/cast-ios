//
//  LeftSlideVC.swift
//  CastApp
//
//  Created by brst on 8/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit


enum LeftMenu: Int {
    
    case FindCasteesVc = 0
    case InviteCasteesVC
    case SettingsVC
    case logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
    func reloadMenu()
    
}

class LeftSlideVC: BaseViewController,LeftMenuProtocol{

//OUTLETS
    
    @IBOutlet var tableViewMenu: UITableView!
    
    
    
//Variables
    var menus = ["Home","Invite Castees","Settings","Logout"]

    var findCasteeVC: UIViewController!
    var settingsVC: SettingsVC!
    var visibleVC: UIViewController!
    var inviteCasteesVC: UIViewController!
   // var scheduleGigVC:UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    
    }
    
// MARK: CustomizeUI
    func customizeUI()  {
        let storyboard = UIStoryboard(storyboard: .Main)
        
        findCasteeVC = storyboard.instantiateViewController(withIdentifier: "FindCasteesVC") as! FindCasteesVC
        settingsVC = storyboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        inviteCasteesVC = storyboard.instantiateViewController(withIdentifier: "InviteCasteesVC") as! InviteCasteesVC
        //  scheduleGigVC = storyboard.instantiateViewController(withIdentifier: "ScheduleGigVC") as! ScheduleGigVC
        
        settingsVC.leftMenuProtocol = self
        visibleVC = UIApplication.topViewController()
        
        // check the role of the user
        if DataManager.isCastee == true {
            menus.remove(at: 1)
        }else{
            if !menus.contains("Invite Castees") {
                menus.insert("Invite Castees", at: 1)
            }
        }
        
    }

    
    func changeViewController(_ menu: LeftMenu) {
     switch menu {
        case .FindCasteesVc:
//        self.slideMenuController()?.changeMainViewController(self.FindCasteeVC, close: false)
            self.slideMenuController()?.closeLeft()
       
        case .InviteCasteesVC:
        
            self.slideMenuController()?.closeLeft()
            visibleVC?.navigationController?.pushViewController(self.inviteCasteesVC, animated: false)
        
        case .SettingsVC:
     
            self.slideMenuController()?.closeLeft()
            visibleVC?.navigationController?.pushViewController(self.settingsVC, animated: false)
        
//     case .ScheduleGigVC:
//        
//        self.slideMenuController()?.closeLeft()
//        visibleVC?.navigationController?.pushViewController(self.scheduleGigVC, animated: false)
        
        case .logout:
       
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
      }

    }
    
    
    func reloadMenu(){
        if DataManager.isCastee == true {
            menus.remove(at: 1)
        }else{
            if !menus.contains("Invite Castees") {
                menus.insert("Invite Castees", at: 1)
            }
        }
        
        tableViewMenu.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension LeftSlideVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //check the role of the user
        if DataManager.isCastee == true{
            var index = indexPath.row
            if indexPath.row != 0 {
                index = indexPath.row+1
            }
            if let menu = LeftMenu(rawValue: index) {
                self.changeViewController(menu)
            }
        }else{
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
                self.changeViewController(menu)
            }
        }
        
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if self.tableView == scrollView {
//            
//        }
//    }
}

extension LeftSlideVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text = menus[indexPath.row]
      
        return cell
    }
}





