//
//  PostGigVC.swift
//  CastApp
//
//  Created by brst on 8/30/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class PostGigVC: BaseViewController,UITextFieldDelegate {

    
    @IBOutlet var txtDate: UITextField!
    
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblDay: UILabel!
    @IBOutlet var lblMonthYear: UILabel!
    
    @IBOutlet var txtStartTime: UITextField!
    
    @IBOutlet var txtEndTime: UITextField!
    
  //VARIABLES
    
    var selectedTextFeild: UITextField!
    var browsingVC:BrowsingVC!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()  
        // Do any additional setup after loading the view.
    }

//MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
        hideTabbar()
        pickerDelegate = self // self the Picker delegate
        let profileStoryboard = UIStoryboard(storyboard:.Profile)
        browsingVC = profileStoryboard.instantiateViewController(withIdentifier: "BrowsingVC") as! BrowsingVC
  
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        backButtonAction()
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        animationPushWithViewController(viewController: browsingVC)
    }
    
    
    //MARK:
    //MARK: UItextfeild Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        selectedTextFeild = textField
        
        if textField == txtDate {
            setDatePickerView(textField: txtDate, mode: 1)
        }else if (textField == txtStartTime){
            
            setDatePickerView(textField: txtStartTime, mode: 0)
        }else{
            setDatePickerView(textField: txtEndTime, mode: 0)
        }
    }
    
    
}

//MARK:
//MARK: Picker View Delegates
extension PostGigVC: BaseVCDelegate{
    func didSelectPickerViewAtIndex(index: Int) {
        
        
    }
   func didSelectDatePicker(date: Date)
    {
        if selectedTextFeild == txtDate {
        
        lblDay.text = date.dayOfWeek()!
        let dateString = date.stringFormDateInLocalTimeZone(format: .dmyDate)
        let dateArray  = dateString.components(separatedBy: "-")
        lblDate.text = dateArray[0]
        lblMonthYear.text = dateArray[1].uppercased()
            
        }else if selectedTextFeild == txtStartTime{
            
            
        }else{
           
            
        }
    }
    func didClickOnDoneButton() {
        
        
    }
}




