//
//  FindCastee0sVC.swift
//  CastApp
//
//  Created by brst on 8/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class FindCasteesVC: BaseViewController {
//OUTLETS
    
    @IBOutlet var collectionViewProfile: UICollectionView!
    
    @IBOutlet var tableViewCastees: UITableView!
    
    @IBOutlet var collectionViewGigs: UICollectionView!
    
    @IBOutlet var btnAvailableOrReccomOutlet: UIButton!
    
    @IBOutlet var lblNavigationTitle: UILabel!
    
 //VARIABLES
    let numberArray = ["1","2","3","4","5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    
//MARK: CusomizeUI
    func customizeUI()  {
        hideNavigationBar()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
        layout.scrollDirection = .horizontal

       // layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = (UIScreen.main.bounds.width-240)/2
        collectionViewGigs!.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupTheView()
    }
    
    func setupTheView()
    {
        // check the user is castee or caster
        if DataManager.isCastee == true {
            lblNavigationTitle.text = kFindGigs
            btnAvailableOrReccomOutlet.setTitle(kAvailableGigs, for: .normal)
        }else{
            lblNavigationTitle.text = kFindCastee
            btnAvailableOrReccomOutlet.setTitle(kReccomendedCastee, for: .normal)
        }
        
        collectionViewProfile.reloadData()
        tableViewCastees.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 //MARK: Buttons Action
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }

    @IBAction func btnSearchAction(_ sender: Any) {
        
            animationPushWithIdentifier(identifier: kFindCasteeToCalender)
    }
    
    @IBAction func btnAvailableOrReccomendedAction(_ sender: Any) {
        //Check the role of the user
        if DataManager.isCastee == true {
            animationPushWithIdentifier(identifier: "FindCasteeToAvailableGig")
        }else{
            
        }
    }
    
}
//MARK: UICollection View Methods
extension FindCasteesVC: UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    
    //MARK: Collection view DetaSource Methods
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // check the collection view
        if collectionView == collectionViewProfile {
            return 4
        }else{
            return numberArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //check the collection view
        if collectionView == collectionViewProfile {
            return CGSize(width: collectionViewProfile.bounds.size.width, height:collectionViewProfile.bounds.size.height)
        }else{
            return CGSize(width: 60 , height:60)
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell:UICollectionViewCell!
        if collectionView == collectionViewProfile {
          cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath as IndexPath)
            let imgProfile = cell.viewWithTag(10) as! UIImageView
            let gigView = cell.viewWithTag(30)
            let lblDesc = cell.viewWithTag(20) as! UILabel
            if DataManager.isCastee == true{
                imgProfile.isHidden = true
                gigView?.isHidden = false
                lblDesc.text = "Lead Role, Darama Film"
            }else{
                imgProfile.isHidden = false
                gigView?.isHidden = true
                lblDesc.text = "John Doe, 22"
            }
            
        }else{
          cell = collectionView.dequeueReusableCell(withReuseIdentifier: kGigsCell, for: indexPath as IndexPath)
            let lblNumber = cell.viewWithTag(10) as! UILabel
            lblNumber.text = numberArray[indexPath.row] 
        }
        
        return cell
    }
 
}
//MARK: UITableView Methods
extension FindCasteesVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath) as! FindCasteeCell
        customizeCell(indexPath: indexPath, cell: cell)
        
        return cell
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:FindCasteeCell)  {
        
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = UIColor.init(hex: kLightPurple, Alpha: 0.9)
        }else{
            cell.contentView.backgroundColor = UIColor.init(hex: kPurple, Alpha: 0.9)
        }
    
        if DataManager.isCastee == true {
            cell.lblDay.isHidden = false
            cell.lblName.text      = "Rebeca's Gig"
            cell.lblRoleOrAge.text = "Supporting Role"
            cell.lblDay.text       = "Monday"
            cell.constraintLeadingOfRole.constant = 17
        }else{
            cell.lblDay.isHidden = true
            cell.lblName.text      = "Sharon"
            cell.lblRoleOrAge.text = "22"
            cell.lblDay.text       = ""
            cell.constraintLeadingOfRole.constant = 25
        }
    }
}
//MARK: SliderMenu Delegate Methods
extension CustomTabBarVC : SlideMenuControllerDelegate {

    
    func leftDidClose() {
        if UserVM.sharedInstance.isLogout == true {
            self.showAlert(message: kLogoutString, title: "", otherButtons: [kYes:{action in
                //self.Logout()
                UserVM.sharedInstance.isLogout = false
                self.logout()
                
                }], cancelTitle: kNo, cancelAction:{ (action) in
                    UserVM.sharedInstance.isLogout = false
            })
        }
    }
 
}

