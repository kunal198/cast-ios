//
//  CreateGigVC.swift
//  CastApp
//
//  Created by brst on 9/14/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CreateGigVC: BaseViewController {

  //OUTLETS
    
    
    
    @IBOutlet var txtSelectDate: datePickerTextField!
    
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var lblDay: UILabel!
    
    @IBOutlet var lblMonthYear: UILabel!
    @IBOutlet var txtGigTitle: UITextField!
    
    @IBOutlet var txtGigDescription: UITextView!
    
    @IBOutlet var txtSpecificRequriments: UITextView!
    
    @IBOutlet var btnCreateApplicationOutlet: UIButton!
    
    @IBOutlet var btnPostGigOutlet: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }

    
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
        hideTabbar()
        btnCreateApplicationOutlet.setButtonGrayBorderLayer(radius:8.0,alpha: 0.6) // set the signin button border
        btnPostGigOutlet.setBorderCornerRadius(radius: 30.0) // set the signin button border
        
        // set the placeholder color
        txtGigTitle.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 12, style: .regular)
      
        pickerDelegate = self // self the Picker delegate
        setDatePickerView(textField: txtSelectDate, mode: 1)
        
    }
 //MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    @IBAction func btnCreateAppliactionAction(_ sender: Any) {
        
        animationPushWithIdentifier(identifier: "createGigToCreateApplication")
    }
    
    
    @IBAction func btnPostGigAction(_ sender: Any) {
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


//MARK:
//MARK: Picker View Delegates
extension CreateGigVC: BaseVCDelegate{
    func didSelectPickerViewAtIndex(index: Int) {
        
        
    }
    func didSelectDatePicker(date: Date)
    {
            lblDay.text = date.dayOfWeek()!
            let dateString = date.stringFormDateInLocalTimeZone(format: .dmyDate)
            let dateArray  = dateString.components(separatedBy: "-")
            lblDate.text = dateArray[0]
            lblMonthYear.text = dateArray[1].uppercased()

        
    }
    
    func didClickOnDoneButton() {
        
        
    }
}






