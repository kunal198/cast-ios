//
//  BrowsingVC.swift
//  CastApp
//
//  Created by brst on 9/4/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class BrowsingVC: BaseViewController {

//OUTLETS
    
    @IBOutlet var collectionViewCastees: UICollectionView!
    
    @IBOutlet var txtKeywordSearch: UITextField!

    
    //VARIABLES
    
    var isAge       = true
    var isSex       = false
    var isRating    = true
    var isLocation  = false
    var isHeight    = true
    var isBodytype  = false
    var isHairColor = true
    var isSkinColor = false

    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
    
//MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
        // set the placeholder color
        txtKeywordSearch.setPlaceholder(color: UIColor.init(hex: kPurple, Alpha: 1.0), size: 12, style: .regular)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK:
//MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
        
    }
    
    @IBAction func btnAgeAction(_ sender: UIButton) {
        
        if isAge == true {
            isAge = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isAge = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
        
    }
    @IBAction func btnSexAction(_ sender: UIButton) {
        if isSex == true {
            isSex = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isSex = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
    }
    @IBAction func btnRatingAction(_ sender: UIButton) {
        if isRating == true {
            isRating = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isRating = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
    }
   
    @IBAction func btnLocationAction(_ sender: UIButton) {
        
        if isLocation == true {
            isLocation = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isLocation = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
    }
    
    @IBAction func btnHeightAction(_ sender: UIButton) {
        
        if isHeight == true {
            isHeight = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isHeight = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
    }
    
    @IBAction func btnBodytypeAction(_ sender: UIButton) {
        if isBodytype == true {
            isBodytype = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isBodytype = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
    }
    @IBAction func btnHairColorAction(_ sender: UIButton) {
        if isHairColor == true {
            isHairColor = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isHairColor = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
        
    }
    
    @IBAction func btnSkinColorAction(_ sender: UIButton) {
        if isSkinColor == true {
            isSkinColor = false
            sender.setImage(#imageLiteral(resourceName: "unCheckBox"), for: .normal)
        }else{
            isSkinColor = true
            sender.setImage(#imageLiteral(resourceName: "CheckBox"), for: .normal)
        }
        
    }
}

//MARK:
//MARK: UICollection View Methods
extension BrowsingVC: UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    
    //MARK: Collection view DetaSource Methods
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 20
        }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize(width:(collectionViewCastees.bounds.width-15)/4 , height:((collectionViewCastees.bounds.width-15)/4)+26)
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath as IndexPath)
        customizeCell(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
  //MARK: custom cell method
    func customizeCell(cell:UICollectionViewCell,indexPath:IndexPath)  {
        
        let borderColor: CGColor! = UIColor.init(hex: KWhiteColor, Alpha: 1.0).cgColor
        let borderWidth: CGFloat = 0.6
        cell.layer.borderWidth = borderWidth
        cell.layer.borderColor = borderColor
        let imgStar = cell.viewWithTag(10) as! UIImageView
        //set the top image
        if indexPath.row%2 == 0 {
            imgStar.image = #imageLiteral(resourceName: "addBrowsingButton")
        }else{
            imgStar.image = #imageLiteral(resourceName: "browsingStar")
        }
        
        
    }
}


