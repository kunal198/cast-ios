//
//  ScheduleGigVC.swift
//  CastApp
//
//  Created by brst on 8/31/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ScheduleGigVC:BaseViewController {
//OUTLETS
    
    @IBOutlet var txtStartTime: UITextField!
    
    @IBOutlet var txtEndTime: UITextField!
   
    @IBOutlet var lblStartTime: UILabel!
  
    @IBOutlet var lblStartYear: UILabel!
    
    @IBOutlet var lblStartDay: UILabel!
    
    @IBOutlet var lblStartDate: UILabel!
    
    @IBOutlet var lblStartMonth: UILabel!
    
    @IBOutlet var lblEndTime: UILabel!
   
    @IBOutlet var lblEndYear: UILabel!
    
    @IBOutlet var lblEndDay: UILabel!
    
    @IBOutlet var lblEndDate: UILabel!
    
    @IBOutlet var lblEndMonth: UILabel!
    
    
//VARIABLES
    var selectedTextFeild: UITextField!
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
//MARK: CustomizeUI
    
    func customizeUI()  {
        hideNavigationBar()
        hideTabbar()
        pickerDelegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  //MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    
    @IBAction func btnScheduleAction(_ sender: Any) {
        
        animationPushWithIdentifier(identifier: "scheduleToApplication")
    }
    
    //MARK:
    //MARK: UItextfeild Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        selectedTextFeild = textField
        setDatePickerView(textField: textField, mode: 2)

//        if textField == txtStartTime {
//            setDatePickerView(textField: txtStartTime, mode: 2)
//        }else{
//            setDatePickerView(textField: txtEndTime, mode: 2)
//        }
    }
    
}

//MARK:
//MARK: Picker View Delegates
extension ScheduleGigVC: BaseVCDelegate{
    
    func didSelectDatePicker(date: Date)
    {
        if selectedTextFeild == txtStartTime {
            
            lblStartDay.text = date.dayOfWeek()
            let dateString = date.stringFromDate(format: .dMyDate)
            let dateArray  = dateString.components(separatedBy: "-")
            lblStartDate.text = dateArray[0]
            lblStartMonth.text = dateArray[1].uppercased()
            lblStartYear.text = dateArray[2]
            lblStartTime.text = dateArray[3]
            
        }else{
            
            lblEndDay.text = date.dayOfWeek()
            let dateString = date.stringFromDate(format: .dMyDate)
            let dateArray  = dateString.components(separatedBy: "-")
            lblEndDate.text = dateArray[0]
            lblEndMonth.text = dateArray[1].uppercased()
            lblEndYear.text = dateArray[2]
            lblEndTime.text = dateArray[3]
            
        }
        
    }
    func didClickOnDoneButton() {
        
        
    }
    
    
}


