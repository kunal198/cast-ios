//
//  CreateApplicationFormVC.swift
//  CastApp
//
//  Created by brst on 9/15/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CreateApplicationFormVC: BaseViewController {

//OUTLETS
    
    @IBOutlet var btnUserNameOutlet: UIButton!
    
    @IBOutlet var btnZipCodeOutlet: UIButton!
    
    @IBOutlet var btnLinkToDemoOutlet: UIButton!
    
    @IBOutlet var btnVideoCastOutlet: UIButton!
    
    @IBOutlet var btnAddQuestionOutlet: UIButton!
    
    @IBOutlet var btnFinishOutlet: UIButton!
 
//VARIABLES
    
    var isUserName      = true
    var isZipCode       = false
    var isLinkToDemo    = false
    var IsVideoCast     = true
    
    
    
    
//MARK: ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
       
    }
//MARK: CustomizeUI
    func customizeUI()  {
        
        btnAddQuestionOutlet.setBorderCornerRadius(radius: 15.0)// set the Add question button border
        btnFinishOutlet.setBorderCornerRadius(radius: 30.0) // set the Finish button border
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
        
    }
    @IBAction func btnUserNameAction(_ sender: Any) {
        
        if isUserName == true {
            isUserName = false
            btnUserNameOutlet.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }else{
            isUserName = true
            btnUserNameOutlet.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        }
        
        
    }
    
    @IBAction func btnZipCodeAction(_ sender: Any) {
        
        if isZipCode == true {
            isZipCode = false
            btnZipCodeOutlet.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }else{
            isZipCode = true
            btnZipCodeOutlet.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        }
        
        
    }
    
    @IBAction func btnLinkToDemoAction(_ sender: Any) {
        
        if isLinkToDemo == true {
            isLinkToDemo = false
            btnLinkToDemoOutlet.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }else{
            isLinkToDemo = true
            btnLinkToDemoOutlet.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        }
        
    }

    @IBAction func btnVideoCastAction(_ sender: Any) {
        if IsVideoCast == true {
            IsVideoCast = false
            btnVideoCastOutlet.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }else{
            IsVideoCast = true
            btnVideoCastOutlet.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        }
    }
    
    @IBAction func btnAddQuestionAction(_ sender: Any) {
        
        
    }
    
    @IBAction func btnFinishAction(_ sender: Any) {
        
        
    }
    
    
    
    
}
