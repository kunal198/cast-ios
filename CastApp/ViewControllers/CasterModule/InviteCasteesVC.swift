//
//  InviteCasteesVC.swift
//  CastApp
//
//  Created by brst on 8/29/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class InviteCasteesVC: BaseViewController {
//OUTLET
    
    @IBOutlet var tableViewCastees: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
//MARK:customizeUI

    func customizeUI()  {
        
        hideTabbar()
        hideNavigationBar()
        self.tableViewCastees.estimatedRowHeight = 70    //Minimum Row Height
        self.tableViewCastees.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  //MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    

}

//MARK: UITableView Methods
extension InviteCasteesVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        customizeCell(indexPath: indexPath, cell: cell)
        
        return cell
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = UIColor.init(hex: kLightPurple, Alpha: 0.9)
        }else{
            cell.contentView.backgroundColor = UIColor.clear
        }
    }
}

//MARK: UICollection View Methods
extension InviteCasteesVC: UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    
    //MARK: Collection view DetaSource Methods
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 4
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 138 , height:105)
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath as IndexPath)
        
        return cell
    }
    
}


