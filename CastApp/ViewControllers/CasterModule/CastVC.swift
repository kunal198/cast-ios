//
//  CastVC.swift
//  CastApp
//
//  Created by brst on 8/31/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CastVC: BaseViewController {

 //OUTLETS
    
    @IBOutlet var btnBookedOutlet: UIButton!
    
    @IBOutlet var btnAppliedOutlet: UIButton!
    
    @IBOutlet var btnAllOutlet: UIButton!
    
    @IBOutlet var lblLeftSeprator: UILabel!
    
    @IBOutlet var lblRightSeprator: UILabel!
   
    @IBOutlet var txtSearch: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
//MARK: CustomizeUI
    func customizeUI()  {
            hideNavigationBar()
            txtSearch.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 20, style: .regular)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 //MARK: Buttons Action

    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    
    
    @IBAction func btnBookedAction(_ sender: Any) {
        
        btnBookedOutlet.backgroundColor = UIColor.white
        btnAppliedOutlet.backgroundColor = UIColor.clear
        btnAllOutlet.backgroundColor = UIColor.clear
        btnBookedOutlet.setTitleColor(UIColor.init(hex: "95628F", Alpha: 1.0), for: .normal)
        btnAppliedOutlet.setTitleColor(UIColor.white, for: .normal)
        btnAllOutlet.setTitleColor(UIColor.white, for: .normal)

        lblLeftSeprator.isHidden = true
        lblRightSeprator.isHidden = false
        
    }
    
    @IBAction func btnAppliedAction(_ sender: Any) {
        
        btnAppliedOutlet.backgroundColor = UIColor.white
        btnBookedOutlet.backgroundColor = UIColor.clear
        btnAllOutlet.backgroundColor = UIColor.clear
        btnAppliedOutlet.setTitleColor(UIColor.init(hex: "95628F", Alpha: 1.0), for: .normal)
        btnAllOutlet.setTitleColor(UIColor.white, for: .normal)
        btnBookedOutlet.setTitleColor(UIColor.white, for: .normal)
        lblLeftSeprator.isHidden = true
        lblRightSeprator.isHidden = true
        
    }
    
    @IBAction func btnAllAction(_ sender: Any) {
        
        btnAllOutlet.backgroundColor = UIColor.white
        btnAppliedOutlet.backgroundColor = UIColor.clear
        btnBookedOutlet.backgroundColor = UIColor.clear
        btnAllOutlet.setTitleColor(UIColor.init(hex: "95628F", Alpha: 1.0), for: .normal)
        btnAppliedOutlet.setTitleColor(UIColor.white, for: .normal)
        btnBookedOutlet.setTitleColor(UIColor.white, for: .normal)
        lblLeftSeprator.isHidden = false
        lblRightSeprator.isHidden = true
        
        
    }

}
//MARK: UITableView Methods
extension CastVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        customizeCell(indexPath: indexPath, cell: cell)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = UIColor.init(hex: "6A536E", Alpha: 0.9)
        }else{
            cell.contentView.backgroundColor = UIColor.init(hex: "5B425F", Alpha: 0.9)
        }
        
    }
}


