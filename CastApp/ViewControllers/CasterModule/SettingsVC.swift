//
//  SettingsVC.swift
//  CastApp
//
//  Created by brst on 8/28/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit



class SettingsVC: BaseViewController {

    //OUTLETS
    
    @IBOutlet var tableViewSettings: UITableView!
    
//VARIABLES

    let headingArray = ["Role","Name","E-Mail","Bio","Location","Agency","Notifications","About"]
    let dataArray = ["","","CasterEmail@carstudos.com","Caster's Bio// Caster's Bio// Caster's Bio// Caster's Bio//","","","",""]
    var isCastee = DataManager.isCastee
    var isNotification = false
    var leftMenuProtocol:LeftMenuProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
    
//MARK: CoustomizeUI
    
    func customizeUI()  {
        hideNavigationBar()
        self.tableViewSettings.estimatedRowHeight = 51    //Minimum Row Height
        self.tableViewSettings.rowHeight = UITableViewAutomaticDimension
    }
  
//MARK: Buttons Action
    
    @IBAction func btnMenuAction(_ sender: Any) {
        backButtonAction()
    }
 
    @IBAction func btnLogoutAction(_ sender: Any) {
        self.showAlert(message: kLogoutString, title: "", otherButtons: [kYes:{action in
            self.logout()
            }], cancelTitle: kNo, cancelAction:{ (action) in
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
//MARK: UITableView Methods
extension SettingsVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "RoleCell", for: indexPath)
            let btnSwitch = cell.viewWithTag(10) as! UIButton
            btnSwitch.addTarget(self, action:#selector(self.btnCasteeSwitch), for: .touchUpInside)
            //set the image for caste or castee
            if isCastee == true {
                btnSwitch.setImage(#imageLiteral(resourceName: "RoleSwitchCastee"), for: .normal)
            }else{
                btnSwitch.setImage(#imageLiteral(resourceName: "RoleSwitchCaster"), for: .normal)
            }
            
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
            customizeCell(indexPath: indexPath, cell: cell)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
   //MARK:Switch Actions
    
    func btnCasteeSwitch()  {
        if isCastee == true {
            DataManager.isCastee = false
            isCastee = false

        }else{
            DataManager.isCastee = true
            isCastee = true
        }
        
        //call the left menu delegate.
        leftMenuProtocol?.reloadMenu()
        // Post notification for the profile View of the tabbar
        NotificationCenter.default.post(name: Notification.Name("checkUserRole"), object: nil)
        tableViewSettings.reloadData() // reload the tableview
    }
    
  //MARK:
 //MARK: Notification Button Action
    func btnNotificationSwitch()  {
        
        if  isNotification == true {
            isNotification = false
        }else{
            isNotification = true
        }
        tableViewSettings.reloadData()
    }
 //MARK: Customize Cell Method
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        
        let lblheading = cell.viewWithTag(10) as! UILabel
        let lblData = cell.viewWithTag(20) as! UILabel
        let btnSwitch = cell.viewWithTag(30) as! UIButton
        let imgArrow = cell.viewWithTag(40) as! UIImageView
        
        lblheading.text = headingArray[indexPath.row]
        lblData.text = dataArray[indexPath.row]
         btnSwitch.addTarget(self, action:#selector(self.btnNotificationSwitch), for: .touchUpInside)
        // set the notification buttton and about image
        if indexPath.row == 6 {
            btnSwitch.isHidden = false
            imgArrow.isHidden = true
            //set the notification switch image
            if isNotification == true {
                btnSwitch.setImage(#imageLiteral(resourceName: "NotificationSwitchOn"), for: .normal)
            }else{
                btnSwitch.setImage(#imageLiteral(resourceName: "NotificationSwitchOff"), for: .normal)
            }
        }else if indexPath.row == 7{
            btnSwitch.isHidden = true
            imgArrow.isHidden = false
        }else{
            btnSwitch.isHidden = true
            imgArrow.isHidden = true
        }
     }
}







