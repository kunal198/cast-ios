//
//  NotificationVC.swift
//  CastApp
//
//  Created by brst on 8/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class NotificationVC: BaseViewController {
//OUTLETS
    
    @IBOutlet var tableViewNotification: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coustomizeUI()
        // Do any additional setup after loading the view.
    }
//MARK: CoustomizeUI
    func coustomizeUI()  {
        hideNavigationBar()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//MARK: Buttons Action
    
    @IBAction func btnMenuAction(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
    }
    
}
extension NotificationVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: kHeaderCell)
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        let imgSeprator = cell.viewWithTag(90) as! UIImageView
        // hide the last seprator
        if indexPath.row == 3 {
            imgSeprator.isHidden = true
        }else{
            imgSeprator.isHidden = false
        }
        return cell
    }
    
}

