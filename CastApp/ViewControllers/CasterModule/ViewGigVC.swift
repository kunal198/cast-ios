//
//  ViewGigVC.swift
//  CastApp
//
//  Created by brst on 8/29/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ViewGigVC: BaseViewController {

    //OUTLETS
    @IBOutlet var lblCasterStatus: UILabel!
    
    @IBOutlet var lblPostedName: UILabel!
    
    @IBOutlet var lblReturnOrApply: UILabel!
    
    @IBOutlet var btnViewCasteOutlet: UIButton!
    
    @IBOutlet var btnEndGigOutlet: UIButton!
    
    @IBOutlet var constraintLeadinglblCasterStatus: NSLayoutConstraint!
    
    @IBOutlet var imgStatus: UIImageView!
    
    @IBOutlet var lblAvalable: UILabel!
    
//VARIABLES
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
//MARK: customizeUI
    func customizeUI()  {
        hideTabbar()
        hideNavigationBar()
        // set the castee and caster View
        if DataManager.isCastee == true {
            lblCasterStatus.text = "2 / 8 Role Filled"
            lblPostedName.text          = "John Giggs"
            imgStatus.isHidden          = false
            lblAvalable.isHidden        = false
            btnViewCasteOutlet.isHidden = true
            btnEndGigOutlet.isHidden    = true
            constraintLeadinglblCasterStatus.constant = 140
            lblReturnOrApply.text       = "Apply Now"
            lblReturnOrApply.font       = lblReturnOrApply.font.withSize(19)
        }else{
            
            lblPostedName.text          = "Me"
            imgStatus.isHidden          = true
            lblAvalable.isHidden        = true
            btnViewCasteOutlet.isHidden = false
            btnEndGigOutlet.isHidden    = false
            constraintLeadinglblCasterStatus.constant = 8
            lblReturnOrApply.text       = "Return"
            lblReturnOrApply.font       = lblReturnOrApply.font.withSize(12)
            lblCasterStatus.text = "2 / 8 Role Filled, 6 Remaining"

        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    
    @IBAction func btnViewCasteeAction(_ sender: Any) {
        animationPushWithIdentifier(identifier: kViewgigToCastVC)
        
    }
   
    @IBAction func btnEndGigAction(_ sender: Any) {
    }
  
    @IBAction func btnApplyOrReturnAction(_ sender: Any) {
        
        // Check the role of the user
        if DataManager.isCastee == true  {
            animationPushWithIdentifier(identifier: kViewGigToViewQuestions)
        }else{
            backButtonAction()
        }
    }

}
