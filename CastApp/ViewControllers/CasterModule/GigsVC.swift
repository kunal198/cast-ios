//
//  GigsVC.swift
//  CastApp
//
//  Created by brst on 8/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class GigsVC: BaseViewController {
//OUTLETS
    
    @IBOutlet var constraintHeightOfbtnCreateNew: NSLayoutConstraint!
    
    
    
// VARIABLES
    let headerArray = ["Open","Filled"]
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
    
//MARK: coustomizeUI
    
    func customizeUI()  {
        hideNavigationBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showTabbar()
        // Check the role of the user
        if DataManager.isCastee == true  {
            constraintHeightOfbtnCreateNew.constant = 0
        }else{
            constraintHeightOfbtnCreateNew.constant = 102
        }
    }
    
 //MARK: Buttons Action
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
   
    @IBAction func btnCreateNew(_ sender: Any) {
        animationPushWithIdentifier(identifier: kGigToScheduleGig)
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension GigsVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerArray.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: kHeaderCell)
        let lblHeading = headerCell?.viewWithTag(10) as! UILabel
        lblHeading.text = headerArray[section]
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
    
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        animationPushWithIdentifier(identifier: kGigsToViewGigs)
    }
}




