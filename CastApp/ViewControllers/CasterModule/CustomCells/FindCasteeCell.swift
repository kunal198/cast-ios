//
//  FindCasteeCell.swift
//  CastApp
//
//  Created by brst on 8/29/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class FindCasteeCell: UITableViewCell {
//OUTLETS
    @IBOutlet var imgProfile: UIImageView!
    
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var lblRoleOrAge: UILabel!
    
    @IBOutlet var lblDay: UILabel!
    
    @IBOutlet var constraintLeadingOfRole: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
