//
//  AddVideoQuestionCell.swift
//  CastApp
//
//  Created by brst on 9/1/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class AddVideoQuestionCell: UITableViewCell {
//OUTLETS
    
    @IBOutlet var lblQuestionNumber: UILabel!
    
    @IBOutlet var txtQuestion: UITextView!
    
    @IBOutlet var btnMinusOutlet: UIButton!
    
    @IBOutlet var btnPlusOutlet: UIButton!
    @IBOutlet var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
