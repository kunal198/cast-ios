//
//  CasterProfileVC.swift
//  CastApp
//
//  Created by brst on 8/31/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CasterProfileVC: BaseViewController {
//OUTLETS
    

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        
    }
    
//MARK: customizeUI
    func customizeUI()  {
        
        hideNavigationBar()
        
    }
 //MARK: Buttons Action
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        self.showAlert(message: kLogoutString, title: "", otherButtons: [kYes:{action in
            self.logout()
            
            }], cancelTitle: kNo, cancelAction:{ (action) in
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
