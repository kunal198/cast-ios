//
//  AddVideoQuestionVC.swift
//  CastApp
//
//  Created by brst on 9/1/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class AddVideoQuestionVC: BaseViewController {

 //OUTLETS
    
    @IBOutlet var questionTableView: UITableView!
    
 //VARIABLES
    var questionsArray = ["Question 1",""]
    var timeArray = ["60"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
    
//MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
    
    }
    
//MARK: Buttons Action
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


//MARK: UITableView Methods
extension AddVideoQuestionVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == questionsArray.count-1{
           return 190
        }else{
            return 100
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == questionsArray.count-1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell", for: indexPath)
            let btnAddQuestion = cell.viewWithTag(10) as! UIButton
                btnAddQuestion.addTarget(self, action:#selector(AddVideoQuestionVC.addQuestionAction), for: .touchUpInside)
            let btnSubmit = cell.viewWithTag(20) as! UIButton
                btnSubmit.addTarget(self, action:#selector(AddVideoQuestionVC.btnSubmitAction), for: .touchUpInside)
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath) as! AddVideoQuestionCell
            customizeCell(indexPath: indexPath, cell: cell)
            return cell
        }
    }
    
    //Buttons action
    func addQuestionAction()  {
        
        questionsArray.insert("Question \(questionsArray.count)", at: questionsArray.count-1)
          timeArray.insert("60", at: timeArray.count)
        questionTableView.reloadData()
    }
    
    func btnSubmitAction()  {
        animationPushWithIdentifier(identifier: "videoQuestionToPostGig")
    }
    
    func btnPlusAction(sender:UIButton)  {
        var currentNumber = Int(timeArray[sender.tag])
        currentNumber = currentNumber!+1
        
        timeArray.remove(at: sender.tag)
        timeArray.insert("\(currentNumber!)", at: sender.tag)
        let indexPath = IndexPath(item: sender.tag, section: 0)
        questionTableView.reloadRows(at: [indexPath], with: .none)
        
    }
    
    func btnMinusAction(sender:UIButton)  {
        
        var currentNumber = Int(timeArray[sender.tag])
        
        if currentNumber != 0 {
            currentNumber = currentNumber!-1
            timeArray.remove(at: sender.tag)
            timeArray.insert("\(currentNumber!)", at: sender.tag)
            let indexPath = IndexPath(item: sender.tag, section: 0)
            questionTableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:AddVideoQuestionCell)  {
        
         cell.btnPlusOutlet.addTarget(self, action:#selector(AddVideoQuestionVC.btnPlusAction), for: .touchUpInside)
         cell.btnMinusOutlet.addTarget(self, action:#selector(AddVideoQuestionVC.btnMinusAction), for: .touchUpInside)
         cell.btnPlusOutlet.tag = indexPath.row
         cell.btnMinusOutlet.tag = indexPath.row
         cell.lblTime.text = timeArray[indexPath.row]
        
    }
    
}
