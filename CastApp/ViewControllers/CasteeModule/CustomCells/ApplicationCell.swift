//
//  ApplicationCell.swift
//  CastApp
//
//  Created by brst on 8/29/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ApplicationCell: UITableViewCell {
//OUTLETS
    @IBOutlet var lblHeading: UILabel!
   
    @IBOutlet var btnTap: UIButton!
    
    @IBOutlet var txtEnterSkils: UITextView!
    
    @IBOutlet var imgRightArrow: UIImageView!
    @IBOutlet var imgLeftArrow: UIImageView!
    
//VARIABLES
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
