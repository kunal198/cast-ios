//
//  CalenderTableViewCell.swift
//  CastApp
//
//  Created by brst on 9/1/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CalenderTableViewCell: UITableViewCell {
//OUTLETS
    
    @IBOutlet var lblDayName: UILabel!
    
    @IBOutlet var btnTickOutlet: UIButton!
    
    @IBOutlet var btnCrossOutlet: UIButton!
    
    @IBOutlet var txtAvailableTime: UITextField!
    
    @IBOutlet var lblAvailableTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
