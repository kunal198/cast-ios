//
//  AvailableGigVC.swift
//  CastApp
//
//  Created by brst on 8/30/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class AvailableGigVC: BaseViewController {

//OUTLETS
    @IBOutlet var txtDate: UITextField!
    
    @IBOutlet var tableViewGigs: UITableView!
    
    @IBOutlet var lblDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    
//MARK: customizeUI
    func customizeUI()  {
        hideTabbar()
        hideNavigationBar()
        pickerDelegate = self
        setDatePickerView(textField: txtDate, mode: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 //MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
}

//MARK: UITableView Methods
extension AvailableGigVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        customizeCell(indexPath: indexPath, cell: cell)
        return cell
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        let imgCheckGig = cell.viewWithTag(10) as! UIImageView
        let lblGigTitle = cell.viewWithTag(20) as! UILabel
        
        if indexPath.row == 2 {
            imgCheckGig.image = #imageLiteral(resourceName: "uncheckGig")
            lblGigTitle.alpha = 0.4
        }else{
            imgCheckGig.image = #imageLiteral(resourceName: "CheckGig")
            lblGigTitle.alpha = 1.0
        }
    }
}


//MARK:
//MARK: Picker View Delegates
extension AvailableGigVC: BaseVCDelegate{
  
    func didSelectDatePicker(date: Date)
    {
        
//            .text = date.dayOfWeek()!
//            let dateString = date.stringFormDateInLocalTimeZone(format: .dmyDate)
//            let dateArray  = dateString.components(separatedBy: "-")
//            lblDate.text = dateArray[0]
//            lblMonthYear.text = dateArray[1].uppercased()
        
       
    }
    func didClickOnDoneButton() {
        
        
}


}
