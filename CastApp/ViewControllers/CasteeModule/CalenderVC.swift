//
//  CalenderVC.swift
//  CastApp
//
//  Created by brst on 9/1/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CalenderVC: BaseViewController,FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {

//OUTLETS
    
    @IBOutlet var constraintTopOfAddButton: NSLayoutConstraint!
    
    @IBOutlet var constraintHeightOfCalender: NSLayoutConstraint!
    
    @IBOutlet var calenderView: FSCalendar!
 
    @IBOutlet var lblAvailableOrJobSchedule: UILabel!
 
    @IBOutlet var lblNotAvailOrNoJobSchedule: UILabel!
    
    @IBOutlet var lblColorNotAvailable: UILabel!
    
    @IBOutlet var lblGigOrjobInfo: UILabel!
    
    @IBOutlet var btnAddGigOrAddJobOutlet: UIButton!
    
//VARIABLES

    let availableDateArray = ["09-18-2017","09-19-2017","09-20-2017","09-12-2017","09-12-2017","09-11-2017","09-22-2017","10-02-2017","09-01-2017","09-02-2017","09-03-2017","09-04-2017","09-10-2017"]
    
    var createGigVC:CreateGigVC!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    
//MARK: customizeUI
    func customizeUI()  {
//         calenderView.allowsMultipleSelection = true
        
        btnAddGigOrAddJobOutlet.setBorderCornerRadius(radius: 17.5)
        let gigStoryBoard = UIStoryboard(storyboard:.Gigs)
        createGigVC = gigStoryBoard.instantiateViewController(withIdentifier: "CreateGigVC") as! CreateGigVC
        //check the user role
        if DataManager.isCastee == true{
            lblAvailableOrJobSchedule.text = "Available"
            lblNotAvailOrNoJobSchedule.text = "Not Available"
            btnAddGigOrAddJobOutlet.setTitle("Add a Gig to Calendar", for: .normal)
            lblColorNotAvailable.backgroundColor = UIColor.init(hex: "E80007", Alpha: 1.0)  // red
            
        }else{
            lblAvailableOrJobSchedule.text = "Job scheduled"
            lblNotAvailOrNoJobSchedule.text = "No jobs scheduled"
            btnAddGigOrAddJobOutlet.setTitle("Add a Job to Calendar", for: .normal)
            lblColorNotAvailable.backgroundColor = UIColor.white // white
            
        }
        // Check for the 5s
        
        if self.view.bounds.width == 320 {
            constraintTopOfAddButton.constant = 18
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK: Buttons Action
    
  
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
  
    @IBAction func btnAddGigOrAddJobAction(_ sender: Any) {
        
        if DataManager.isCastee == true {
            
            
        }else{
            animationPushWithViewController(viewController: createGigVC)
        }
        
    }
    
    
    
//MARK: FSCalendar Delegate Methods
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool)
    {
        self.constraintHeightOfCalender.constant = bounds.height
        self.view.layoutIfNeeded()
    }

    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor?
    {
        let dateString: String = date.stringFromDate(format: .mdyDate)
        
        //Check the user availabe dates
        if availableDateArray.contains(dateString)        {
            
            
            return UIColor.init(hex: "4EFF70", Alpha: 1.0)  // green
        }
       
        else
        {
            // Check the role of the user
            if DataManager.isCastee == true {
                return UIColor.init(hex: "E80007", Alpha: 1.0)  // red

            }else{
                return UIColor.white  // WHITE
 
            }
        }
        
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        
        let dateString = date.stringFromDate(format: .mdyDate)
        
        if availableDateArray.contains(dateString) {
            return UIColor.init(hex: "4EFF70", Alpha: 1.0)
        }else{
            // Check the role of the user
            if DataManager.isCastee == true {
                return UIColor.init(hex: "E80007", Alpha: 1.0)  // red
                
            }else{
                return UIColor.white  // WHITE
                
            }
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        
        let dateString = date.stringFromDate(format: .mdyDate)
        
        if availableDateArray.contains(dateString) {
            return UIColor.init(hex: "4EFF70", Alpha: 1.0)
        }else{
            // Check the role of the user
            if DataManager.isCastee == true {
                return UIColor.init(hex: "E80007", Alpha: 1.0)  // red
                
            }else{
                return UIColor.white  // WHITE
                
            }

        }
        
    }
    
    
    
    
}


