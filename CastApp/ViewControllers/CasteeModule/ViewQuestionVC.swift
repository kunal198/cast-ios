//
//  ViewQuestionVC.swift
//  CastApp
//
//  Created by brst on 9/1/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ViewQuestionVC: BaseViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        
    }
 
 //MARK: CustomizeUI
    
    func customizeUI(){
        hideNavigationBar()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
        
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        
    }
    
    @IBAction func btnPreviousAction(_ sender: Any) {
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        popToRootVC()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
