//
//  ProfileVC.swift
//  CastApp
//
//  Created by brst on 8/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ProfileVC: BaseViewController {

//OUTLETS
    
    @IBOutlet var collectionViewRatings: UICollectionView!
    
    @IBOutlet var collectionViewPhotos: UICollectionView!
    
    @IBOutlet var collectionViewVideos: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
//MARK: CustomizeUI
    
    func customizeUI()  {
        hideNavigationBar()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension ProfileVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: Collection view DetaSource Methods
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // check the collection view
        if collectionView == collectionViewRatings {
            return 4
        }else if collectionView == collectionViewPhotos{
            return 5
        }else{
            return 4
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //check the collection view
        if collectionView == collectionViewRatings {
            return CGSize(width: 120, height:61)
        }else if collectionView == collectionViewPhotos {
            return CGSize(width: (collectionViewPhotos.bounds.width-8)/2 , height:240
            )
        }else{
            return CGSize(width: (collectionViewVideos.bounds.width-10)/2 , height:75
            )
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell:UICollectionViewCell!
        if collectionView == collectionViewRatings {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath as IndexPath)
        }else if collectionView == collectionViewPhotos{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: kPhotoCell, for: indexPath as IndexPath)
//            let lblNumber = cell.viewWithTag(10) as! UILabel
//            lblNumber.text = numberArray[indexPath.row]
        }else{
              cell = collectionView.dequeueReusableCell(withReuseIdentifier: kVideoCell, for: indexPath as IndexPath)
        }
        
        return cell
    }
}

