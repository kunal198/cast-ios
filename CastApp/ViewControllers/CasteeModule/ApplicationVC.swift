//
//  ApplicationVC.swift
//  CastApp
//
//  Created by brst on 8/29/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ApplicationVC: BaseViewController {

    //OUTLETS
    @IBOutlet var tableViewSkils: UITableView!
    
    
    
    
    //VARIABLES
    let headingArray = ["Skills","Experience","Other Experience / Info"]
    
    var selectedIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
    
//MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
        hideTabbar()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  //MARK: Buttons Actions
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
       animationPushWithIdentifier(identifier: kApplicationToAddVideoQuestion)
        
    }

    @IBAction func btnSaveAction(_ sender: Any) {
    }
    
 //MARK:
//MARK: UItextFeilds Delegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        self.selectedIndex = -1
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool{
        
        if textView.text == "" {
            
            DispatchQueue.main.async {
                
                let indexPath = IndexPath(item: textView.tag-100, section: 0)
                self.tableViewSkils.reloadRows(at: [indexPath], with: .none)
            }
        }
        
        return true
    }
    
}

//MARK: UITableView Methods
extension ApplicationVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100
        }
        return 150
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath) as! ApplicationCell
        customizeCell(indexPath: indexPath, cell: cell)
        
        return cell
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:ApplicationCell)  {
        cell.lblHeading.text = headingArray[indexPath.row]

        if indexPath.row == 0 {
            cell.btnTap.setTitle("Tap to enter skills", for: .normal)
        }else{
            cell.btnTap.setTitle("Tap to enter experience", for: .normal)
        }
        cell.btnTap.addTarget(self, action:#selector(self.btnTapAction), for: .touchUpInside)
        cell.btnTap.tag        = indexPath.row
        cell.txtEnterSkils.tag = indexPath.row+100
        //check the placeholder button
        if indexPath.row == selectedIndex || cell.txtEnterSkils.text != ""{
            cell.txtEnterSkils.isHidden = false
            cell.txtEnterSkils.becomeFirstResponder()
            cell.imgRightArrow.isHidden = true
            cell.imgLeftArrow.isHidden = true
            cell.btnTap.isHidden = true
        }else{
            cell.txtEnterSkils.isHidden = true
            cell.imgRightArrow.isHidden = false
            cell.imgLeftArrow.isHidden = false
            cell.btnTap.isHidden = false
        }
        
        
    }
    //Button Action
    func btnTapAction(sender:UIButton)  {
        
        selectedIndex = sender.tag
        let indexPath = IndexPath(item: sender.tag, section: 0)
        tableViewSkils.reloadRows(at: [indexPath], with: .none)
        
    }
    
    
    
    
}

