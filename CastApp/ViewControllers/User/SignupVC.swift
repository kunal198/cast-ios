//
//  SignupVC.swift
//  CastApp
//
//  Created by brst on 8/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class SignupVC: BaseViewController {

    //OUTLETS
    
    @IBOutlet var txtName: UITextField!
    
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var txtPassword: UITextField!
    
    @IBOutlet var txtPhone: UITextField!
    
    @IBOutlet var btnContinueOutlet: UIButton!
    
    @IBOutlet var btnCreateEmployerOutlet: UIButton!
    
    
    @IBOutlet var btnProfileOutlet: UIButton!
 //VARIABLE
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coustomizeUI()
        // Do any additional setup after loading the view.
    }
//MARK: CoustomizeUI
    
    func coustomizeUI()  {
        
        btnContinueOutlet.setBorderCornerRadius(radius: 30.0) // set the signin button border
        btnCreateEmployerOutlet.setBorderCornerRadius(radius: 30.0) // set the signin button border

        // set the placeholder color
        txtName.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
        txtEmail.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
        txtPassword.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
        txtPhone.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: Buttons Action
    
    @IBAction func btnSignInAction(_ sender: Any) {
        backButtonAction()
        
    }
    @IBAction func btnCreateEmployerAction(_ sender: Any) {
        
        animationPushWithIdentifier(identifier: kSignUpToCreateEmployer)
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        //  Image
        self.view.endEditing(true)
        CustomImagePickerView.sharedInstace.imageSize = btnProfileOutlet.frame.size
        self.imagePickerDelegate = self
        self.showImagePicker()

    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        // Login to the application
        appDelegate.createMenu()
    }

}

//MARK: Custom Image Picker Delegates
extension SignupVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {   //Get Image from Picker and use the image
        btnProfileOutlet.setImage(image, for: .normal)
//        imageDict["profile_image"] = image.jpegData(.low)
    }
}


