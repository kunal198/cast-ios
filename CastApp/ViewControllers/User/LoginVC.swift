//
//  LoginVC.swift
//  CastApp
//
//  Created by brst on 8/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class LoginVC: BaseViewController {

    //OUTLETS
    
    @IBOutlet var txtName: UITextField!
    
    @IBOutlet var txtPassword: UITextField!
    
    @IBOutlet var btnSignInOutlet: UIButton!
    
    @IBOutlet var constraintTopLogoImg: NSLayoutConstraint!
    
 //VARIABLES
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    
 //MARK:View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()  
        // Do any additional setup after loading the view.
    }
    
//MARK: CostomizeUI
    func customizeUI()  {
        setStatusBarLight()
        hideNavigationBar()
        btnSignInOutlet.setBorderCornerRadius(radius: 30.0) // set the signin button border
        // set the placeholder color
        txtName.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
        txtPassword.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
    }
//MARK: Buttons Action
    

    @IBAction func btnSignUpAction(_ sender: Any) {
        animationPushWithIdentifier(identifier: kLoginToSignUP)
    }
    
    @IBAction func btnSignInAction(_ sender: Any) {
        
        appDelegate.createMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
}
