//
//  CreateEmployerVC.swift
//  CastApp
//
//  Created by brst on 9/13/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CreateEmployerVC: BaseViewController {

//OUTLETS
    
    @IBOutlet var btnCompanyLogoOutlet: UIButton!
    
    @IBOutlet var txtName: UITextField!
    
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var txtCompanyBackground: UITextView!
    
    @IBOutlet var btnAddJobOutlet: UIButton!
    
    @IBOutlet var btnContinueOutlet: UIButton!
    
    
//VARIABLES
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coustomizeUI()
    }
    
    
//MARK: CustomizeUI
    func coustomizeUI()  {
        
        btnContinueOutlet.setBorderCornerRadius(radius: 15.0) // set the signin button border
        btnAddJobOutlet.setBorderCornerRadius(radius: 15.0) // set the signin button border
        
        // set the placeholder color
        txtName.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
        txtEmail.setPlaceholder(color: UIColor.init(hex: KWhiteColor, Alpha: 1.0), size: 14, style: .regular)
        txtCompanyBackground.textColor = UIColor.init(hex: KWhiteColor, Alpha: 1.0)
        
    }
 //MARK: Buttons Action
    
    @IBAction func btnAddJobAction(_ sender: Any) {
        
        
    }
    
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
        appDelegate.createMenu() // login to the application
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()  // back button action
        
    }
    
    @IBAction func btnCompanyLogoAction(_ sender: Any) {
        
        //  Image  picker
        self.view.endEditing(true)
        CustomImagePickerView.sharedInstace.imageSize = btnCompanyLogoOutlet.frame.size
        self.imagePickerDelegate = self
        self.showImagePicker()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}



//MARK: Custom Image Picker Delegates
extension CreateEmployerVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {   //Get Image from Picker and use the image
        btnCompanyLogoOutlet.setImage(image, for: .normal)
        //        imageDict["profile_image"] = image.jpegData(.low)
    }
}


