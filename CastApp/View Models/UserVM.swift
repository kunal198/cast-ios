//
//  UserVM.swift
//  HealthSplashPatient
//
//  Created by 123 on 13/04/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import Foundation


class UserVM {
    
    public static let sharedInstance = UserVM()
    private init() {}

    //array for Insurance List
    var insuranceListArray = [NSDictionary]()
    var isLogout = Bool()
    
    func signupByEmail(signupDict: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.signupByEmail(signupDetails: signupDict, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! Bool == true {
                self.parseUserData(responseDict: responseDict!, type: "signup")
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func loginWithEmail(loginDetails: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.loginWithEmail(loginDetails:loginDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! Bool == true {
                self.parseUserData(responseDict: responseDict!, type: "login")
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    
}
